export interface BaseNodeData {
  readonly title: string; // Title to display
  readonly text: string; // Scene text to display
  readonly image?: string; // Optional background image
  readonly acquireOnEnter?: Inventory; // Items that are acquired upon entering scene
  readonly loseOnEnter?: Inventory; // Items that are lost upon entering scene
}

// Current dialogue scene
export interface NodeData extends BaseNodeData {
  choices: NodeOption[];
} // All the choices that can be made from this scene

// NodeData as Json
export interface JsonNodeData extends BaseNodeData {
  choices?: JsonOption[]; // All the choices that can be made from this scene
}

export interface BaseOption {
  readonly text: string; // Display text describing the next scene
  requiredItems?: Inventory; // items needed to choose option
  disabledText?: string; // Text to display if option is disabled
  hideOnFailure?: boolean; // Should option be hidden if requirements aren't met
}

// Information about a choice the player can make
export interface NodeOption extends BaseOption {
  data: NodeData; // The next scene to go to
}

// Option data as Json
export interface JsonOption extends BaseOption {
  id: number; // The id of the next scene to go to
}

export interface LoaderOutput {
  nodes: JsonNodeData[];
  inventory: Inventory;
}

// Player inventory that contains a map of
// the names of items to the number of that item contained
export type Inventory = Record<string, number>;
