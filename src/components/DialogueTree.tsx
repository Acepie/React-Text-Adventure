import * as React from "react";
import { Inventory, JsonOption, LoaderOutput, NodeData } from "../dialogue";
import { DialogueNode } from "./DialogueNode";

export interface TreeProps {
  dataLoader: () => LoaderOutput;
}

// Stores the current node to render as well as the users current inventory
// If no current node then the game data has not been loaded yet
export interface TreeState {
  currentNode?: NodeData;
  inventory: Inventory;
}

export class DialogueTree extends React.PureComponent<TreeProps, TreeState> {
  constructor(props: TreeProps) {
    super(props);
    this.state = { inventory: {} };
  }

  // uses dataLoader to get json data and convert it into node tree
  // sets first node in list to start node
  public componentWillMount() {
    const nodes: NodeData[] = [];
    const allChoicesList: JsonOption[][] = [];

    const { nodes: nodeData, inventory } = this.props.dataLoader();
    nodeData.forEach(node => {
      const { choices, ...nodeBase } = node;
      nodes.push({ ...nodeBase, choices: [] });
      allChoicesList.push(choices || []);
    });

    // relink nodes using choices list
    for (let i = 0; i < allChoicesList.length; i++) {
      const choiceList = allChoicesList[i];

      nodes[i].choices = choiceList.map(choice => {
        const { id, hideOnFailure, ...choiceBase } = choice;
        return {
          ...choiceBase,
          data: nodes[id],
          hideOnFailure: Boolean(hideOnFailure)
        };
      });
    }

    this.setState({ currentNode: nodes[0], inventory });
  }

  public render() {
    let display = null;
    if (!this.state.currentNode) {
      display = <div>Loading</div>;
    } else {
      display = (
        <DialogueNode
          data={this.state.currentNode}
          inventory={this.state.inventory}
          continue={this.chooseNewNode.bind(this)}
        />
      );
    }
    return <div className="dialogue-tree">{display}</div>;
  }

  public chooseNewNode(currentNode: NodeData) {
    this.setState(prev => {
      const newInventory = { ...prev.inventory };
      if (currentNode.loseOnEnter) {
        Object.keys(currentNode.loseOnEnter).forEach(item => {
          if (newInventory[item]) {
            newInventory[item] -= currentNode.loseOnEnter[item];
            if (!newInventory[item] || newInventory[item] < 0) {
              delete newInventory[item];
            }
          }
        });
      }
      if (currentNode.acquireOnEnter) {
        Object.keys(currentNode.acquireOnEnter).forEach(item => {
          if (newInventory[item]) {
            newInventory[item] += currentNode.acquireOnEnter[item];
          } else {
            newInventory[item] = currentNode.acquireOnEnter[item];
          }
        });
      }
      return { currentNode, inventory: newInventory };
    });
  }
}
