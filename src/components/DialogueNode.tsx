import * as React from "react";
import { Inventory, NodeData, NodeOption } from "../dialogue";

export interface NodeProps {
  data: NodeData;
  inventory: Inventory;
  continue: (currentNode: NodeData) => any;
}

export class DialogueNode extends React.PureComponent<NodeProps> {
  constructor(props: NodeProps) {
    super(props);
  }

  public render() {
    return (
      <div className="dialogue-node">
        <div className="dialogue-text">
          <h1>{this.props.data.title}</h1>
          <p>{this.props.data.text}</p>
          <ul>
            {this.props.data.choices.map(choice => {
              const passes = this.meetsRequirements(
                choice,
                this.props.inventory
              );
              return !passes && choice.hideOnFailure ? null : (
                <li key={choice.text}>
                  <button
                    disabled={!passes}
                    onClick={this.handleClick(choice.data)}
                  >
                    {passes ? choice.text : choice.disabledText || choice.text}
                  </button>
                </li>
              );
            })}
          </ul>
        </div>
        {this.props.data.image ? (
          <div className="dialogue-image">
            <img src={this.props.data.image} />
          </div>
        ) : null}
        <div className="dialogue-inventory">
          <h2>Inventory</h2>
          <ul>
            {Object.keys(this.props.inventory).map(item => {
              return (
                <li key={item}>
                  {item} : {this.props.inventory[item]}
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }

  private meetsRequirements(choice: NodeOption, inv: Inventory): boolean {
    return (
      !choice.requiredItems ||
      Object.keys(choice.requiredItems).every(item => {
        return (
          Object.keys(inv).includes(item) &&
          inv[item] >= choice.requiredItems[item]
        );
      })
    );
  }

  private handleClick(newData: NodeData) {
    return () => {
      this.props.continue(newData);
    };
  }
}
