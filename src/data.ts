import { LoaderOutput } from "./dialogue";

export default function dataLoader(): LoaderOutput {
  return {
    nodes: [
      {
        title: "The Show",
        text:
          "Oh hello, I'm glad you could make it tonight. I've heard some really good things about the performer",
        choices: [
          {
            id: 1,
            text: "The performer?"
          }
        ]
      },
      {
        title: "The Performer",
        text:
          "Yes, a lovely young pianist from the city. Are you ready to go in? The performance is about to begin.",
        choices: [
          {
            id: 2,
            text: "Let's get going",
            requiredItems: { Ticket: 1 },
            disabledText: "I seem to be missing a ticket"
          },
          {
            id: 3,
            text: "Give me a minute"
          }
        ]
      },
      {
        title: "Seating",
        text:
          "Well these are lovely seats. I'm so excited for the show to start.",
        choices: [
          {
            id: 3,
            text: "Actually I need a minute"
          }
        ]
      },
      {
        title: "The Lobby",
        text: "Alright don't be too long",
        choices: [
          {
            id: 2,
            text: "Let's get going",
            requiredItems: { Ticket: 1 },
            disabledText: "I seem to be missing a ticket"
          },
          {
            id: 4,
            text: "Buy a ticket",
            requiredItems: { Coin: 500 },
            disabledText: "I don't have enough money"
          }
        ]
      },
      {
        title: "Buying a ticket",
        text: "Excellent choice. Enjoy the show!",
        acquireOnEnter: { Ticket: 1 },
        loseOnEnter: { Coin: 500 },
        choices: [
          {
            id: 2,
            text: "Let's get going",
            requiredItems: { Ticket: 1 },
            disabledText: "I seem to be missing a ticket"
          }
        ]
      }
    ],
    inventory: {
      Coin: 1000
    }
  };
}
